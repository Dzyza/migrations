<?php


use Phinx\Seed\AbstractSeed;

class FillTableAvailable extends AbstractSeed
{
    public function run()
    {
        $faker = Faker\Factory::create('ru_RU');
        $data = [];
        for($i = 0; $i <1000; $i++)
        {
            $data[] =
                [
                    'available' =>$faker->unixTime(1000)
                ];
        }
        $this->insert('available',$data);
    }
}
