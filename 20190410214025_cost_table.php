<?php


use Phinx\Migration\AbstractMigration;

class CostTable extends AbstractMigration
{
    public function change()
    {
        $posts = $this->table('cost',['primary_key'=>'id']);

        $posts
            ->addColumn('cost', 'integer',['limit'=>50,'null'=>false])
            ->create();
    }
}
