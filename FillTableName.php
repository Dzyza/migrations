<?php


use Phinx\Seed\AbstractSeed;

class FillTableName extends AbstractSeed
{
    public function run()
    {
        $faker = Faker\Factory::create('ru_RU');
        $data = [];
        for($i = 0; $i <1000; $i++)
        {
            $data[] =
                [
                    'name' =>$faker->sentence(mt_rand(2,6)),
                ];
        }
        $this->insert('name',$data);
    }
}
