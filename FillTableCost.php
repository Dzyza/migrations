<?php


use Phinx\Seed\AbstractSeed;

class FillTableCost extends AbstractSeed
{
    public function run()
    {
        $faker = Faker\Factory::create('ru_RU');
        $data = [];
        for($i = 0; $i <1000; $i++)
        {
            $data[] =
                [
                    'cost' =>$faker->unixTime(1000)
                ];
        }
        $this->insert('cost',$data);
    }
}
