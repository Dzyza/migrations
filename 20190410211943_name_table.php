<?php


use Phinx\Migration\AbstractMigration;

class NameTable extends AbstractMigration
{
    public function change()
    {
        $posts = $this->table('name',['primary_key'=>'id']);

        $posts
              ->addColumn('name', 'text',['limit'=>50,'null'=>false])
              ->addColumn('date','datetime',['default'=>CURRENT_TIMESTAMP,'null'=>false])
              ->create();
    }
}
