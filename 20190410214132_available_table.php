<?php


use Phinx\Migration\AbstractMigration;

class AvailableTable extends AbstractMigration
{
    public function change()
    {
        $posts = $this->table('available',['primary_key'=>'id']);

        $posts
            ->addColumn('available', 'integer',['limit'=>50,'null'=>false])
            ->create();
    }
}
